const express = require("express");
const app = express();
// grabbing a http variable from the http library from node.js
// need it to help build the server with socket.io
const http = require("http");
// socket.io deals with a lot of cors issues
const cors = require("cors");

// import from the socket.io library
// Need curly brackets because it's something that exists
const { Server } = require("socket.io");
// helps resolves problems with cors/socket.io
app.use(cors());

const server = http.createServer(app);
// Instantiate the server by creating an io and connection that we're going to establish
// equal to a new instance of the server
// first pass the server we created
const io = new Server(server, {
    cors: {
        // What is an origin? Telling our Server which server is going to be calling
        // Making the calls to uor socket.io server. React server which is going to be running
        origin: "http://localhost:3000",
        // we can specify what methods we would accept
        // helps resolves some of the cors issues
        methods: ["GET", "POST"],
    },
});

// io.on is listening for an event with this id "connection"
// everything we write with socket.io will be inside of this
io.on("connection", (socket) => {
    console.log(`User Connected: ${socket.id}`);

    // when it emits an event join the room based on it's id 
    // You can pass stuff as data
    socket.on("join_room", (data) => {
        socket.join(data);
        console.log(`User with ID: ${socket.id} joined room: ${data}`);
      });


// event to send messageData. This events receives data from messageData
    // Want to only emit to people in the room
    // Where We are sending it
    socket.on("send_message", (data) => {
        socket.to(data.room).emit("receive_message", data);
        console.log(data, "I am Batman.");
      });


// listening for a disconnect. Disconnecting from the server like closing the tab
socket.on("disconnect", () => {
    console.log("User Disconnected", socket.id);
  });
});

server.listen(3001, () => {
    //When the server is running
    console.log("Server is Running. Anime is awesome.");
});

