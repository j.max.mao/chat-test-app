import './App.css';
import io from 'socket.io-client';
import React, { useState } from "react"; 
import Chat from './Chat';

//Connecting front and back end
const socket = io.connect("http://localhost:3001");

function App() {
  //create a state represent username and room
  // Auto import from react
  const [username, setUsername] = useState("");
  const [room, setRoom] = useState("");
  const [showChat, setShowChat] = useState(false);
// Function to join a room when they click on the button
// establish a connection between the user who logged in and room they'd like to enter
const joinRoom = () => {
  if (username !== "" && room !== "") {
//For us to know its the id and not random data, we need to pass it in the front end 
// to pass the data we need to pass it as the second argument room
// This is what its going to be received as data
socket.emit("join_room", room);
setShowChat(true);
}
};


return (
  <div className="App">
    {!showChat ? (
      <div className="joinChatContainer">
        <h3>Join A Chat</h3>
      <input type="text" placeholder="TikTok" 
      // Target.value accessing the value of the input
      onChange={(event) => {
              setUsername(event.target.value);
      }}/>

      <input type="text" placeholder="Anime Room id" 
            onChange={(event) => {
              setRoom(event.target.value);
      }}/>
          <button onClick={joinRoom}>Join A Room</button>
        </div>
      ) : (
        <Chat socket={socket} username={username} room={room} />
      )}
    </div>
  );
}

export default App;
